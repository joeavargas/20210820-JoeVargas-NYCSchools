//
//  Results.swift
//  20210820-JoeVargas-NYCSchools
//
//  Created by Joe Vargas on 8/20/21.
//

import Foundation

struct Results {
    var schoolName: String
    var testTakers: String?
    var readingScore: String?
    var mathScore: String?
    var writingScore: String?
    
    init(raw: [String]) {
        schoolName = raw[0]
        testTakers = raw[1]
        readingScore = raw[2]
        mathScore = raw[3]
        writingScore = raw[4]
    }
}

func loadCSV(from csvName: String) -> [Results]{
    var csvToStruct = [Results]()
    
    // locate the csv file
    guard let filePath = Bundle.main.path(forResource: csvName, ofType: "csv") else {
        return []
    }
    
    // convert contents of the file into one very long string
    var data = ""
    do {
        data = try String(contentsOfFile: filePath)
    } catch {
        return []
    }
    
    // split the long string into an array of "rows" of data. Each row is a string
    // detect "/n" carriage return, then split
    var rows = data.components(separatedBy: "\n")
    
    // remove header rows
    rows.removeFirst()
    
    // loop through each row and split into columns
    for row in rows {
        let csvColumns = row.components(separatedBy: ",")
        let resultStruct = Results.init(raw: csvColumns)
        csvToStruct.append(resultStruct)
    }
    
    return csvToStruct
}
