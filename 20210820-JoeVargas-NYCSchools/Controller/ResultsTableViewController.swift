//
//  ResultsTableViewController.swift
//  20210820-JoeVargas-NYCSchools
//
//  Created by Joe Vargas on 8/20/21.
//

import UIKit

class ResultsTableViewController: UITableViewController {

    var results = loadCSV(from: SAT_RESULTS_CSV)
    var data = [Results]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationController?.navigationBar.prefersLargeTitles = true
        self.title = "Schools"
        
        
        for item in results {
            data.append(item)
        }
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "toSchoolInfoVC" {
            let indexPath = tableView.indexPathForSelectedRow!
            let data = data[indexPath.row]
            let schoolInfoVC = segue.destination as! SchoolInfoViewController

            schoolInfoVC.data = data
        }
    }
    

}
// MARK: - Table view data source
extension ResultsTableViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell", for: indexPath)
        
        let result = data[indexPath.row]
        cell.textLabel?.text = result.schoolName

        return cell
    }
    

 
}
