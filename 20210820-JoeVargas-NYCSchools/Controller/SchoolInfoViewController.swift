//
//  SchoolInfoViewController.swift
//  20210820-JoeVargas-NYCSchools
//
//  Created by Joe Vargas on 8/20/21.
//

import UIKit

class SchoolInfoViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var numberOfTestTakersLabel: UILabel!
    @IBOutlet weak var avgReaderScoreLabel: UILabel!
    @IBOutlet weak var avgMathScoreLabel: UILabel!
    @IBOutlet weak var avgWriterScoreLabel: UILabel!
    
    // Properties
    var data: Results!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        
        guard let schoolData = data else {return}

        displaySchoolData(schoolData: schoolData)
        
    }
    
    func displaySchoolData(schoolData: Results){
        self.title = schoolData.schoolName
        numberOfTestTakersLabel.text = data.testTakers
        avgReaderScoreLabel.text = data.readingScore
        avgMathScoreLabel.text = data.mathScore
        avgWriterScoreLabel.text = data.writingScore
    }
}
